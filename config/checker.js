module.exports = {
  isAuthenticated: (req, res, next) => {
    // console.log(req.user)
    if (req.user) {
      return next()
    }
    else {
      console.log('Not authorized!')
      return res.status(401).send({
        error: 'User not authenticated'
      })
    }
  }
}