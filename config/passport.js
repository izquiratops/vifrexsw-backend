const LocalStrategy = require('passport-local').Strategy
const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')

const User = mongoose.model('user')

module.exports = function(passport){
	passport.use(new LocalStrategy({usernameField: 'email'}, 
	function(email, password, done) {
		// Looking for the username
		User.findOne({
			email: email
		}).then(user => {
			if (!user) {
				return done(null, false, {message: 'User not found'})
			}
			// Match the password
			bcrypt.compare(password, user.password, (err, isMatch) => {
				if (err) throw err
				if (isMatch) {
					return done(null, user)
				} else {
					return done(null, false, {message: 'Wrong Password'})
				}
			})
		})
	}))

	passport.serializeUser(function(user, done) {
		done(null, user.id)
	})

	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			 done(err, user)
		})
	})
}