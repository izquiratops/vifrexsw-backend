const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

require("../models/Fabric")
require("../models/Supplier")
const Fabric = mongoose.model("fabric")
const Supplier = mongoose.model("supplier")
const ObjectId = require('mongoose').Types.ObjectId

router.post('/post', (req, res) => {
    const input_fabric = req.body
    if (input_fabric.supplier &&
        input_fabric.construction &&
        input_fabric.type &&
        input_fabric.fabric_supplier_code &&
        input_fabric.width &&
        input_fabric.composition &&
        input_fabric.weight &&
        input_fabric.date &&
        input_fabric.vifrex_id) {
        var document = {
            supplier: input_fabric.supplier,
            construction: input_fabric.construction,
            type: input_fabric.type,
            fabric_supplier_code: input_fabric.fabric_supplier_code,
            width: input_fabric.width,
            weight: input_fabric.weight,
            date: input_fabric.date,
            vifrex_id: input_fabric.vifrex_id,
            composition: input_fabric.composition
        }
    } else {
        return res.send({ 'error': "There are empty required fields" })
    }

    if (typeof input_fabric.finishing !== 'undefined') {
        document["finishing"] = input_fabric.finishing
    }
    if (typeof input_fabric.comments !== 'undefined') {
        document["comments"] = input_fabric.comments
    }
    if (typeof input_fabric.price !== 'undefined') {
        document["price"] = input_fabric.price
    }
    if (typeof input_fabric.vifrex_price !== 'undefined') {
        document["vifrex_price"] = input_fabric.vifrex_price
    }
    if (typeof input_fabric.fabric_description !== 'undefined') {
        document["fabric_description"] = input_fabric.fabric_description
    }

    const newFabric = new Fabric(document)
    Supplier.findOne({ supplier_code: input_fabric.supplier }, function (err, obj) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            if(obj){
                newFabric.save((err, doc) => {
                    if (err) {
                        return res.send({ 'error': err })
                    }
                    return res.send({ 'message': doc })
                })
            }else{
                return res.send({ 'error': 'Supplier is not in database' })
            }
        }
    })
})

router.post('/update', (req, res) => {
    const update = req.body.edited_fields
    let id

    try {
        id = new ObjectId(req.body.id)
    }
    catch (err) {
        return res.send(err + '\n')
    }

    const filter = { _id: id }
    Fabric.updateOne(filter, update, { new: false }, function (err, response) {
        if (err) {
            return res.send({ 'error': err })
        }
        return res.send({ 'message': response })
    })
})

router.post('/remove', (req, res) => {
    Fabric.updateOne({ _id: req.body.id, hidden: false }, { 'hidden': true },
    function (err, response) {
        if (err) return res.send({ 'error': err })
        else return res.send({ 'message': response })
    })
})

router.get('/get', (req, res) => {
    const fabric_vifrex_id = req.query.fabric_vifrex_id
    Fabric.findOne({ vifrex_id: fabric_vifrex_id, hidden: false }, function (err, obj) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            return res.send(obj)
        }
    })
})

router.get('/get/check', (req, res) => {
    const fabric_vifrex_id = req.query.fabric_vifrex_id
    const fabric_supplier_code = req.query.fabric_supp_code
    const supplier_code = req.query.supplier_code
    Fabric.findOne({ 
        $or: [{ vifrex_id: fabric_vifrex_id },
        { fabric_supplier_code: fabric_supplier_code, supplier: supplier_code }]
     }, function (err, obj) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            return res.send(obj)
        }
    })
})

router.get('/get/all', (req, res) => {
    Fabric.find({hidden: false}, function(err, fabrics) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let docs_array = []
            fabrics.forEach(element => {
                docs_array.push({
                    "id": element._id.toString(),
                    "supplier": element.supplier,
                    "construction": element.construction,
                    "type": element.type,
                    "fabric_supplier_code": element.fabric_supplier_code,
                    "width": element.width,
                    "finishing": element.finishing,
                    "comments": element.comments,
                    "composition": element.composition,
                    "weight": element.weight,
                    "price": element.price,
                    "vifrex_price": element.vifrex_price,
                    "date": element.date,
                    "vifrex_id": element.vifrex_id,
                    "fabric_description": element.fabric_description
                })
            })
            res.send(docs_array)
        }
    }).sort({ _id: -1 }).limit(100)
})

router.get('/search', (req, res) => {
    const search_field = req.query.search_field
    Fabric.find({
        $or: [{ vifrex_id: { $regex: search_field, $options: 'i' }, hidden: false },
        { supplier: { $regex: search_field, $options: 'i' }, hidden: false },
        { comments: { $regex: search_field, $options: 'i' }, hidden: false },
        { fabric_description: { $regex: search_field, $options: 'i' }, hidden: false },
        { fabric_supplier_code: { $regex: search_field, $options: 'i' }, hidden: false }]
    }, function (err, docs) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let docs_array = []
            docs.forEach(element => {
                docs_array.push({
                    "id": element._id.toString(),
                    "supplier": element.supplier,
                    "construction": element.construction,
                    "type": element.type,
                    "fabric_supplier_code": element.fabric_supplier_code,
                    "width": element.width,
                    "finishing": element.finishing,
                    "comments": element.comments,
                    "composition": element.composition,
                    "weight": element.weight,
                    "price": element.price,
                    "vifrex_price": element.vifrex_price,
                    "date": element.date,
                    "vifrex_id": element.vifrex_id,
                    "fabric_description": element.fabric_description
                })
            })
            res.send(docs_array)
        }
    }).sort({ _id: -1 })
})

module.exports = router;