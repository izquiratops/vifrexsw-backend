const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

require("../models/Fabric")
require("../models/Supplier")
const Fabric = mongoose.model("fabric")
const Supplier = mongoose.model("supplier")
const ObjectId = require('mongoose').Types.ObjectId

router.post('/post', (req, res) => {
    const input_supplier = req.body
    if (input_supplier.supplier_name &&
        input_supplier.supplier_code &&
        input_supplier.country &&
        input_supplier.address &&
        input_supplier.phone_number) {
        var document = {
            supplier_name: input_supplier.supplier_name,
            supplier_code: input_supplier.supplier_code,
            country: input_supplier.country,
            address: input_supplier.address,
            phone_number: input_supplier.phone_number
        }
    } else {
        return res.send(JSON.stringify({ 'error': "There are empty required fields" }))
    }

    if (typeof input_supplier.fax !== 'undefined') {
        document["fax"] = input_supplier.fax
    }
    if (typeof input_supplier.contact_person !== 'undefined') {
        document["contact_person"] = input_supplier.contact_person
    }
    if (typeof input_supplier.postal_code !== 'undefined') {
        document["postal_code"] = input_supplier.postal_code
    }
    if (typeof input_supplier.VAT !== 'undefined') {
        document["VAT"] = input_supplier.VAT
    }

    const newSupplier = new Supplier(document)
    newSupplier.save((err, doc) => {
        if (err) {
            res.status(500)
            return res.send({ 'error': err })
        }
        return res.send({ 'message': doc })
    })
})

router.post('/update', (req, res) => {
    const update = req.body.edited_fields
    let id

    try {
        id = new ObjectId(req.body.id)
    }
    catch (err) {
        return res.send(err + '\n')
    }

    const filter = { _id: id }
    Supplier.findOneAndUpdate(filter, update, { new: false }, function (err, response) {
        if (err) {
            res.status(500)
            return res.send({ 'error': err })
        }
        return res.send({ 'message': response })
    })
})

router.post('/remove', (req, res) => {
    Supplier.updateOne({ _id: req.body.id, hidden: false }, { 'hidden': true },
    function (err, response) {
        if (err) {
            res.status(500)
            return res.send({ 'error': err })
        } else {
            Fabric.updateMany({ supplier: req.body.supplier, hidden: false }, { 'hidden': true },
            function (err, r) {
                if (err) {
                    res.status(500)
                    return res.send({ 'error': err })
                }else {
                    return res.send({ 'message': response })
                }
            })
        }
    })
})

router.get('/get', (req, res) => {
    const supplier_id = req.query.supplier_code
    Supplier.findOne({ supplier_code: supplier_id, hidden: false }, function (err, obj) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            return res.send(obj)
        }
    })
})

router.get('/get/all', (req, res) => {
    Supplier.find({hidden: false}, function(err, suppliers) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let docsF = []
            suppliers.forEach(element => {
                let docsRes = {
                    "id": element._id.toString(),
                    "supplier_name": element.supplier_name,
                    "supplier_code": element.supplier_code,
                    "country": element.country,
                    "address": element.address,
                    "phone_number": element.phone_number,
                    "fax": element.fax,
                    "contact_person": element.contact_person,
                    "postal_code": element.postal_code,
                    "VAT": element.VAT
                }
                docsF.push(docsRes)
            })
            res.send(docsF)
        }
    })
})

router.get('/search', (req, res) => {
    const search_field = req.query.search_field
    Supplier.find({
        $or: [{ supplier_name: { $regex: search_field, $options: 'i' }, hidden: false},
        { supplier_code: { $regex: search_field, $options: 'i' }, hidden: false}]
    }, function (err, docs) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let docsF = []
            docs.forEach(element => {
                let docsRes = {
                    "id": element._id.toString(),
                    "supplier_name": element.supplier_name,
                    "supplier_code": element.supplier_code,
                    "country": element.country,
                    "address": element.address,
                    "phone_number": element.phone_number,
                    "fax": element.fax,
                    "contact_person": element.contact_person,
                    "postal_code": element.postal_code,
                    "VAT": element.VAT
                }
                docsF.push(docsRes)
            })
            res.send(docsF)
        }
    }).limit(10)
})

module.exports = router;