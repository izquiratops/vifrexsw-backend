const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

require("../models/FinalOrder")
const FinalOrder = mongoose.model("finalorder")
require("../models/FinalOrderEntry")
const FinalOrderEntry = mongoose.model("finalorderentry")
require("../models/PaymentTermEntry")
const PaymentTermEntry = mongoose.model("paymenttermentry")
require("../models/SummaryEntry")
const SummaryEntry = mongoose.model("summaryentry")

router.get('/get/all', (req, res) => {
    FinalOrder.find({ hidden: false }, function (err, orders) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            return res.send(orders)
        }
    })
})

router.get('/get/entries', (req, res) => {
    let Entries = []
    FinalOrderEntry.find({ reference: req.query.reference, hidden: false }, function (err, docs) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let finalOrderEntries = []
            docs.forEach(finalorderentry => {
                let finalOrderEntry = {
                    "vifrex_code": finalorderentry.vifrex_code,
                    "color": finalorderentry.color,
                    "length": finalorderentry.length,
                    "ex_mill_date": finalorderentry.ex_mill_date,
                    "eta": finalorderentry.eta,
                    "inco_term": finalorderentry.inco_term,
                    "price": finalorderentry.price,
                    "remove": "",
                }
                finalOrderEntries.push(finalOrderEntry)
            })
            Entries.push(finalOrderEntries)

            PaymentTermEntry.find({ reference: req.query.reference, hidden: false }, function (err, docs) {
                if (err) {
                    res.status(500)
                    return res.send({ error: err })
                } else {
                    let paymentTermEntries = []
                    docs.forEach(paymenttermntry => {
                        let paymentTermEntry = {
                            "payment_term_entry": paymenttermntry.payment_term_entry,
                            "remove": "",
                        }
                        paymentTermEntries.push(paymentTermEntry)
                    })
                    Entries.push(paymentTermEntries)

                    SummaryEntry.find({ reference: req.query.reference, hidden: false }, function (err, docs) {
                        if (err) {
                            res.status(500)
                            return res.send({ error: err })
                        } else {
                            let summaryEntries = []
                            docs.forEach(summaryentry => {
                                let summaryEntry = {
                                    "comments": summaryentry.comments,
                                    "total": '0',
                                }
                                summaryEntries.push(summaryEntry)
                            })
                            Entries.push(summaryEntries)
                            return res.send(Entries)
                        }
                    })
                }
            })
        }
    })
})

router.get('/get/lastcode', (req, res) => {

    let search_field = "PO" + req.query.year + req.query.month + req.query.type;

    FinalOrder.find({ reference: { $regex: search_field, $options: 'i' }, hidden: false }, function (err, orders) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            return res.send(orders)
        }
    }).sort({ _id: -1 }).limit(1)
})

router.post('/post/finalorder', (req, res) => {
    let errorValue;
    let finalOrder = req.body
    
    const input_final_order = finalOrder[0]

    if (input_final_order.reference &&
        input_final_order.supplier_code &&
        input_final_order.date &&
        input_final_order.department &&
        input_final_order.company &&
        input_final_order.client) {
        var document = {
            reference: input_final_order.reference,
            supplier_code: input_final_order.supplier_code,
            date: input_final_order.date,
            client: input_final_order.client,
            department: input_final_order.department,
            company: input_final_order.company,
        }
    } else {
        return res.send(JSON.stringify({ 'error': "There are empty required fields" }))
    }

    const newFinalOrder = new FinalOrder(document)
    newFinalOrder.save((err, doc) => {
        if (err) {
            errorValue = err
        }
    })

    finalOrder.splice(0, 1)

    const input_final_orderentries = finalOrder[0];

    input_final_orderentries.forEach(input_final_orderentry => {
        if (input_final_orderentry.reference &&
            input_final_orderentry.vifrex_code) {
            var document = {
                reference: input_final_orderentry.reference,
                vifrex_code: input_final_orderentry.vifrex_code
            }
        } else {
            return res.send({ 'error': "There are empty required fields" })
        }

        if (typeof input_final_orderentry.ex_mill_date !== 'undefined') {
            document["ex_mill_date"] = input_final_orderentry.ex_mill_date
        }

        if (typeof input_final_orderentry.length !== 'undefined') {
            document["length"] = input_final_orderentry.length
        }

        if (typeof input_final_orderentry.price !== 'undefined') {
            document["price"] = input_final_orderentry.price
        }

        if (typeof input_final_orderentry.eta !== 'undefined') {
            document["eta"] = input_final_orderentry.eta
        }

        if (typeof input_final_orderentry.color !== 'undefined') {
            document["color"] = input_final_orderentry.color
        }

        if (typeof input_final_orderentry.inco_term !== 'undefined') {
            document["inco_term"] = input_final_orderentry.inco_term
        }

        const newFinalOrderEntry = new FinalOrderEntry(document)
        newFinalOrderEntry.save((err, doc) => {
            if (err) {
                errorValue = err
            }
        })
    });

    finalOrder.splice(0, 1)

    const input_summary_entries = finalOrder[0];

    input_summary_entries.forEach(input_summary_entry => {
        if (input_summary_entry.reference) {
            var document = {
                reference: input_summary_entry.reference,
            }
        } else {
            return res.send({ 'error': "There are empty required fields" })
        }

        if (typeof input_summary_entry.comments !== 'undefined') {
            document["comments"] = input_summary_entry.comments
        }

        const newSummaryEntry = new SummaryEntry(document)
        newSummaryEntry.save((err, doc) => {
            if (err) {
                errorValue = err
            }
        })
    });

    finalOrder.splice(0, 1)

    const input_paymentterms_entries = finalOrder[0];

    input_paymentterms_entries.forEach(input_paymentterms_entry => {
        if (input_paymentterms_entry.reference &&
            input_paymentterms_entry.payment_term_entry) {
            var document = {
                reference: input_paymentterms_entry.reference,
                payment_term_entry: input_paymentterms_entry.payment_term_entry
            }
        } else {
            return res.send({ 'error': "There are empty required fields" })
        }

        const newPaymentTermEntry = new PaymentTermEntry(document)
        newPaymentTermEntry.save((err, doc) => {
            if (err) {
                errorValue = err
            }
        })
    });

    if (errorValue == null) {
        return res.send({ 'message': "Final Order added correctly" })
    } else {
        return res.send({ 'error': errorValue })
    }
})

router.post('/update', (req, res) => {
    let errorValue;
    const finalOrder = req.body;
    const reference_po = finalOrder[0].reference;
    finalOrder.splice(0, 1)
    const input_final_order_entries = finalOrder[0]
    FinalOrderEntry.deleteMany({ reference: reference_po }, function (err, r) {
        if (err) {
            res.status(500)
            return res.send({ 'error': err })
        } else {
            input_final_order_entries.forEach(input_final_orderentry => {
                if (input_final_orderentry.reference) {
                    var document = {
                        reference: input_final_orderentry.reference
                    }
                } else {
                    return res.send({ 'error': "There are empty required fields" })
                }

                if (typeof input_final_orderentry.vifrex_code !== 'undefined') {
                    document["vifrex_code"] = input_final_orderentry.vifrex_code
                }

                if (typeof input_final_orderentry.ex_mill_date !== 'undefined') {
                    document["ex_mill_date"] = input_final_orderentry.ex_mill_date
                }

                if (typeof input_final_orderentry.length !== 'undefined') {
                    document["length"] = input_final_orderentry.length
                }

                if (typeof input_final_orderentry.price !== 'undefined') {
                    document["price"] = input_final_orderentry.price
                }

                if (typeof input_final_orderentry.eta !== 'undefined') {
                    document["eta"] = input_final_orderentry.eta
                }

                if (typeof input_final_orderentry.color !== 'undefined') {
                    document["color"] = input_final_orderentry.color
                }

                if (typeof input_final_orderentry.inco_term !== 'undefined') {
                    document["inco_term"] = input_final_orderentry.inco_term
                }

                const newFinalOrderEntry = new FinalOrderEntry(document)
                newFinalOrderEntry.save((err, doc) => {
                    if (err) {
                        errorValue = err
                    }
                })
            });

            finalOrder.splice(0, 1)
            const input_summary_entries = finalOrder[0];
            SummaryEntry.deleteMany({ reference: reference_po }, function (err, r) {
                if (err) {
                    res.status(500)
                    return res.send({ 'error': err })
                } else {
                    input_summary_entries.forEach(input_summary_entry => {
                        if (input_summary_entry.reference) {
                            var document = {
                                reference: input_summary_entry.reference,
                            }
                        } else {
                            return res.send({ 'error': "There are empty required fields" })
                        }

                        if (typeof input_summary_entry.comments !== 'undefined') {
                            document["comments"] = input_summary_entry.comments
                        }

                        const newSummaryEntry = new SummaryEntry(document)
                        newSummaryEntry.save((err, doc) => {
                            if (err) {
                                errorValue = err
                            }
                        })
                    });

                    finalOrder.splice(0, 1)
                    const input_paymentterms_entries = finalOrder[0]
                    PaymentTermEntry.deleteMany({ reference: reference_po }, function (err, r) {
                        if (err) {
                            res.status(500)
                            return res.send({ 'error': err })
                        } else {
                            input_paymentterms_entries.forEach(input_paymentterms_entry => {
                                if (input_paymentterms_entry.reference &&
                                    input_paymentterms_entry.payment_term_entry) {
                                    var document = {
                                        reference: input_paymentterms_entry.reference,
                                        payment_term_entry: input_paymentterms_entry.payment_term_entry
                                    }
                                } else {
                                    return res.send({ 'error': "There are empty required fields" })
                                }

                                const newPaymentTermEntry = new PaymentTermEntry(document)
                                newPaymentTermEntry.save((err, doc) => {
                                    if (err) {
                                        errorValue = err
                                    }
                                })
                            });

                            if (errorValue == null) {
                                return res.send({ 'message': "Order Modified correctly" })
                            } else {
                                return res.send({ 'error': errorValue })
                            }
                        }
                    })

                }
            })
        }
    })
})

router.post('/remove', (req, res) => {
    FinalOrder.updateOne({ reference: req.body.reference, hidden: false }, { 'hidden': true },
    function (error, response) {
        if (error) {
            res.status(500)
            return res.send({ 'error': error })
        } else {
            FinalOrderEntry.updateMany({ reference: req.body.reference, hidden: false } , { 'hidden': true },
            function (err, resp) {
                if (err) {
                    res.status(500)
                    return res.send({ 'error': err })
                } else {
                    SummaryEntry.updateMany({ reference: req.body.reference, hidden: false } , { 'hidden': true },
                    function (err, resp) {
                        if (err) {
                            res.status(500)
                            return res.send({ 'error': err })
                        } else {
                            PaymentTermEntry.updateMany({ reference: req.body.reference, hidden: false } , { 'hidden': true },
                            function (err, resp) {
                                if (err) {
                                    res.status(500)
                                    return res.send({ 'error': err })
                                }
                                return res.send({ 'message': resp })
                            })
                        }
                    })
                }
            })
        }
    })
})

router.get('/search', (req, res) => {
    const search_field = req.query.search_field
    FinalOrder.find({
        $or: [{ reference: { $regex: search_field, $options: 'i' }, hidden: false },
        { client: { $regex: search_field, $options: 'i' }, hidden: false },
        { company: { $regex: search_field, $options: 'i' }, hidden: false },
        { department: { $regex: search_field, $options: 'i' }, hidden: false }]
    }, function (err, docs) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let docsF = []
            docs.forEach(element => {
                let docsRes = {
                    "id": element._id.toString(),
                    "reference": element.reference,
                    "supplier_code": element.supplier_code,
                    "company": element.company,
                    "department": element.department,
                    "client": element.client,
                    "date": element.date,
                }
                docsF.push(docsRes)
            })
            res.send(docsF)
        }
    })
})

module.exports = router;