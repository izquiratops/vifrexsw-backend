const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

require("../models/Client")
const Client = mongoose.model("client")
const ObjectId = require('mongoose').Types.ObjectId
const checker = require('../config/checker')

router.post('/post', checker.isAuthenticated, (req, res) => {
    const input_client = req.body
    if (input_client.company &&
        input_client.department &&
        input_client.name) {
        var document = {
            company: input_client.company,
            department: input_client.department,
            name: input_client.name
        }
    } else {
        return res.send(JSON.stringify({ 'error': "There are empty required fields" }))
    }

    if (typeof input_client.email !== 'undefined') {
        document["email"] = input_client.email
    }
    if (typeof input_client.phone_number !== 'undefined') {
        document["phone_number"] = input_client.phone_number
    }

    const newClient = new Client(document)
    newClient.save((err, doc) => {
        if (err) {
            return res.send({ 'error': err })
        }
        return res.send({ 'message': doc })
    })
})

router.post('/update', checker.isAuthenticated, (req, res) => {
    const update = req.body.edited_fields
    let id

    try {
        id = new ObjectId(req.body.id)
    }
    catch (err) {
        return res.send(err + '\n')
    }

    const filter = { _id: id }
    Client.findOneAndUpdate(filter, update, { new: false }, function (err, response) {
        if (err) {
            return res.send({ 'error': err })
        }
        return res.send({ 'message': response })
    })
})

router.post('/remove', checker.isAuthenticated, (req, res) => {
    Client.updateOne({ _id: req.body.id, hidden: false }, { 'hidden': true },
    function (err, response) {
        if (err) return res.send({ 'error': err })
        else return res.send({ 'message': response })
    })
})

router.get('/get', checker.isAuthenticated, (req, res) => {
    const client_company = req.query.client_company,
        client_department = req.query.client_department,
        client_name = req.query.client_name
    Client.findOne({ name: { $regex: '^' + client_name + '$', $options: 'i' }, 
                     department: { $regex: '^' + client_department + '$', $options: 'i' }, 
                     company: { $regex: '^' + client_company + '$', $options: 'i' },
                    hidden: false }, function (err, obj) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            return res.send(obj)
        }
    })
})

router.get('/get/all', checker.isAuthenticated, (req, res) => {
    Client.find({hidden: false}, function(err, clients) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let docsF = []
            clients.forEach(element => {
                let docsRes = {
                    "id": element._id.toString(),
                    "company": element.company,
                    "department": element.department,
                    "name": element.name,
                    "email": element.email,
                    "phone_number": element.phone_number
                }
                docsF.push(docsRes)
            })
            res.send(docsF)
        }
    })
})

router.get('/search', checker.isAuthenticated, (req, res) => {
    const search_field = req.query.search_field
    Client.find({ department: { $regex: search_field, $options: 'i' }, hidden: false }, function (err, docs) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let docsF = []
            docs.forEach(element => {
                let docsRes = {
                    "id": element._id.toString(),
                    "company": element.company,
                    "department": element.department,
                    "name": element.name,
                    "email": element.email,
                    "phone_number": element.phone_number
                }
                docsF.push(docsRes)
            })
            res.send(docsF)
        }
    }).limit(20)
})

module.exports = router;