const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')

require("../models/Order")
const Order = mongoose.model("order")
require("../models/OrderEntry")
const OrderEntry = mongoose.model("orderentry")
require('../models/Fabric')
const Fabric = mongoose.model("fabric")

router.post('/post/order', (req, res) => {
    let errorValue;
    let order = req.body
    const input_order = order[0]
    if (input_order.reference &&
        input_order.client_company &&
        input_order.client_department &&
        input_order.client_name &&
        input_order.date) {
        var document = {
            reference: input_order.reference,
            client_company: input_order.client_company,
            client_department: input_order.client_department,
            client_name: input_order.client_name,
            date: input_order.date
        }
    } else {
        return res.send(JSON.stringify({ 'error': "There are empty required fields" }))
    }

    const newOrder = new Order(document)
    newOrder.save((err, doc) => {
        if (err) {
            errorValue = err
        }
    })

    order.splice(0, 1)

    const input_orderentries = order;
    input_orderentries.forEach(input_orderentry => {
        if (input_orderentry.reference &&
            input_orderentry.vifrex_id &&
            input_orderentry.sampleyardage &&
            input_orderentry.hanger) {
            var document = {
                reference: input_orderentry.reference,
                vifrex_id: input_orderentry.vifrex_id,
                sampleyardage: input_orderentry.sampleyardage,
                hanger: input_orderentry.hanger
            }
        } else {
            return res.send({ 'error': "There are empty required fields" })
        }

        if (typeof input_orderentry.comments !== 'undefined') {
            document["comments"] = input_orderentry.comments
        }

        const newOrderEntry = new OrderEntry(document)
        newOrderEntry.save((err, doc) => {
            if (err) {
                errorValue = err
            }
        })
    });

    if (errorValue == null) {
        return res.send({ 'message': "Order added correctly" })
    } else {
        return res.send({ 'error': errorValue })
    }
})

router.post('/update', (req, res) => {
    let errorValue;
    const input_orderentries = req.body;
    OrderEntry.deleteMany({ reference: input_orderentries[0].reference }, function (err, r) {
        if (err) {
            res.status(500)
            return res.send({ 'error': err })
        } else {
            input_orderentries.forEach(input_orderentry => {
                if (input_orderentry.reference &&
                    input_orderentry.vifrex_id &&
                    input_orderentry.sampleyardage &&
                    input_orderentry.hanger) {
                    var document = {
                        reference: input_orderentry.reference,
                        vifrex_id: input_orderentry.vifrex_id,
                        sampleyardage: input_orderentry.sampleyardage,
                        hanger: input_orderentry.hanger
                    }
                } else {
                    return res.send({ 'error': "There are empty required fields" })
                }

                if (typeof input_orderentry.comments !== 'undefined') {
                    document["comments"] = input_orderentry.comments
                }

                const newOrderEntry = new OrderEntry(document)
                newOrderEntry.save((err, doc) => {
                    if (err) {
                        errorValue = err
                    }
                })
            });

            if (errorValue == null) {
                return res.send({ 'message': "Order Modified correctly" })
            } else {
                return res.send({ 'error': errorValue })
            }
        }
    })
})

router.get('/get', (req, res) => {
    const query_reference = req.query.reference;
    Order.findOne({ reference: query_reference, hidden: false }, function (err, obj) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            return res.send(obj)
        }
    })
})

router.get('/get/all', (req, res) => {
    Order.find({hidden: false}, function (err, orders) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            return res.send(orders)
        }
    })
})

router.get('/get/entries', (req, res) => {
    OrderEntry.find({ reference: req.query.reference, hidden: false }, function (err, docs) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let docsF = []
            docs.forEach(orderentry => {
                Fabric.findOne({ vifrex_id: orderentry.vifrex_id, hidden: false }, function (err, fabric) {
                    if (err) {
                        res.status(500)
                        return res.send({ error: "Fabric is no longer in database" })
                    } else {
                        if(fabric == null) {
                            console.log(orderentry.vifrex_id)
                        }else {
                            let docsRes = {
                                "sampleyardage": orderentry.sampleyardage,
                                "hanger": orderentry.hanger,
                                "comments_order": orderentry.comments,
                                "vifrex_id": orderentry.vifrex_id,
                                "_id": fabric._id.toString(),
                                "supplier": fabric.supplier,
                                "construction": fabric.construction,
                                "type": fabric.type,
                                "fabric_supplier_code": fabric.fabric_supplier_code,
                                "width": fabric.width,
                                "finishing": fabric.finishing,
                                "comments": fabric.comments,
                                "composition": fabric.composition,
                                "weight": fabric.weight,
                                "price": fabric.price,
                                "date": fabric.date,
                                "fabric_description": fabric.fabric_description,
                                "remove": "",
                            }
                            docsF.push(docsRes)
                        }
                    }
                })
            })
            setTimeout(function () {
                return res.send(docsF)
            }, 3500);
        }
    })
})

router.get('/search', (req, res) => {
    const search_field = req.query.search_field
    Order.find({ reference: { $regex: search_field, $options: 'i' }, hidden: false }, function (err, docs) {
        if (err) {
            res.status(500)
            return res.send({ error: err })
        } else {
            let docsF = []
            docs.forEach(element => {
                let docsRes = {
                    "id": element._id.toString(),
                    "reference": element.reference,
                    "client_company": element.client_company,
                    "client_department": element.client_department,
                    "client_name": element.client_name,
                    "date": element.date,
                }
                docsF.push(docsRes)
            })
            res.send(docsF)
        }
    }).limit(20)
})

router.post('/remove', (req, res) => {
    Order.updateOne({ reference: req.body.reference, hidden: false }, { 'hidden': true },
    function (error, response) {
        if (error) {
            res.status(500)
            return res.send({ 'error': error })
        } else {
            OrderEntry.updateMany({ reference: req.body.reference, hidden: false } , { 'hidden': true },
            function (err, resp) {
                if (err) {
                    res.status(500)
                    return res.send({ 'error': err })
                }
                return res.send({ 'message': resp })
            })
        }
    })
})

module.exports = router;