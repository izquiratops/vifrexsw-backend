const express = require('express')
const session = require('express-session')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const passport = require('passport')
const bcrypt = require('bcryptjs')

const main = express()

// Hay que abrir los puertos para aceptar el tráfico de ENTRADA
// (Siempre que uses localhost puedes sudar de esto)
// $iptables -I INPUT -p tcp --dport 8180 -j ACCEPT 

const PORT = 8180
main.listen(PORT, () => console.log(`CORS-enabled Vifrex web server listening on port ${PORT}!`))

const allowedOrigins = [
    'http://localhost:8100',
]
main.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", allowedOrigins)
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    res.header("Access-Control-Allow-Credentials", true)
    next()
})

// bodyParser IMPORTANTE!!! Permite leer incoming request bodies
main.use(bodyParser.urlencoded({ extended: true }))
main.use(bodyParser.json())

// Express-Session + Passport Middleware
main.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
    maxAge: Date.now() + (30 * 86400 * 1000),
    // store: new MemoryStore(),
}))

main.use(passport.initialize())
main.use(passport.session())

// https://mongoosejs.com/docs/deprecations.html
mongoose.set('useFindAndModify', false)
mongoose.set('useUnifiedTopology', true)

const clients = require('./routes/clients')
const fabrics = require('./routes/fabrics')
const suppliers = require('./routes/suppliers')
const orders = require('./routes/orders')
const finalorders = require('./routes/final_orders')

require("./models/User")
const User = mongoose.model("user")

require('./config/passport')(passport)
main.use('/clients', clients);
main.use('/fabrics', fabrics);
main.use('/suppliers', suppliers);
main.use('/orders', orders);
main.use('/finalorders', finalorders);

// Mongoose Connection
mongoose.connect('mongodb://vifrexGeneric:Vifr3xStud10@167.99.180.12:27017/my-database', { useNewUrlParser: true })
    .then(() => console.log('MongoDB Connected...'))
    .catch(err => console.log(err))
/*mongoose.connect('mongodb://localhost:27017/my-database', { useNewUrlParser: true })
    .then(() => console.log('MongoDB Connected...'))
    .catch(err => console.log(err))*/

const checker = require('./config/checker')
const ObjectId = require('mongoose').Types.ObjectId

main.post('/register', (req, res) => {
    let errors = []
    if (req.body.password.length < 8) {
        errors.push('Your password must be at least 8 characters long.')
    }

    User.findOne({ email: req.body.email }, function (err, obj) {
        if (obj != null) {
            errors.push('This mail is already registered.')
        }
    }).then(() => {
        if (errors.length > 0) {
            return res.send({ 'error': errors[0] })
        } else {
            let newUser = new User({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password,
            })

            bcrypt.genSalt(10, (err, salt) => {
                if (err) throw err
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err
                    newUser.password = hash
                    newUser.save()
                        .then(user => {
                            return res.send({ 'message': 'Register OK' })
                        })
                        .catch(err => {
                            return res.send({ 'error': err })
                        })
                })
            })
        }
    })
})

main.post('/login', function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err) {
            return res.send({ 'error': err })
        }
        if (info) {
            return res.send({ 'error': info['message'] })
        }
        if (!user) {
            return res.send('User not found')
        }
        req.logIn(user, function (err) {
            if (err) {
                return res.send({ 'error': err })
            }
            console.log('login/', req.user)
            return res.send({ 'message': req.session.passport.user })
        })
    })(req, res, next)
})

main.post('/set_password', checker.isAuthenticated, function (req, res) {
    if (req.body.password.length < 8) {
        return res.send({ 'error': 'Your password must be at least 8 characters long.' })
    }

    let id
    try {
        id = new ObjectId(req.body.id)
    }
    catch (err) {
        return res.send(err + '\n')
    }

    const filter = { _id: id }
    bcrypt.genSalt(10, (err, salt) => {
        if (err) throw err
        bcrypt.hash(req.body.password, salt, (err, hash) => {
            if (err) throw err
            const update = { 'password': hash }
            User.findOneAndUpdate(filter, update, { new: false }, function (err, response) {
                if (err) {
                    return res.send({ 'error': err })
                }
                return res.send({ 'message': response })
            })
        })
    })
})

main.get('/logout', checker.isAuthenticated, function (req, res) {
    console.log('User Log Out', req.user.email)
    req.logout()
    return res.send({ 'message': "Logged Out" })
})
