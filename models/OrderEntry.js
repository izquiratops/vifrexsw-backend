const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    reference:{
        type: String,
        required: true
    },
    vifrex_id:{
        type: String,
        required: true
    },
    sampleyardage:{
        type: String,
        required: true
    },
    hanger:{
        type: String,
        required: true
    },
    comments:{
        type: String,
        required: false
    },
    hidden:{
        type: Boolean,
        default: false
    },
})

mongoose.model('orderentry', OrderSchema)