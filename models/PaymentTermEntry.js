const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    reference:{
        type: String,
        required: true
    },
    payment_term_entry:{
        type: String,
        required: true
    },
    hidden:{
        type: Boolean,
        default: false
    },
})

mongoose.model('paymenttermentry', OrderSchema)