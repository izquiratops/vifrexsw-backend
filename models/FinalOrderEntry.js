const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    reference:{
        type: String,
        required: true
    },
    vifrex_code:{
        type: String,
        required: true
    },
    color:{
        type: String,
        required: false
    },
    length:{
        type: String,
        required: false
    },
    ex_mill_date:{
        type: String,
        required: false
    },
    eta:{
        type: String,
        required: false
    },
    inco_term:{
        type: String,
        required: false
    },
    price:{
        type: String,
        required: false
    },
    hidden:{
        type: Boolean,
        default: false
    },
})

mongoose.model('finalorderentry', OrderSchema)