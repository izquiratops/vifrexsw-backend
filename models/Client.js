const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const ClientSchema = new Schema({
    company:{
        type: String,
        required: true
    },
    department:{
        type: String,
        required: true
    },
    name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: false
    },
    phone_number:{
        type: String,
        required: false
    },
    hidden:{
        type: Boolean,
        default: false
    },
})

mongoose.model('client', ClientSchema)