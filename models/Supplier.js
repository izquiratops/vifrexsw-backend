const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const SupplierSchema = new Schema({
    supplier_name:{
        type: String,
        required: true
    },
    supplier_code:{
        type: String,
        required: true
    },
    country:{
        type: String,
        required: true
    },
    address:{
        type: String,
        required: true
    },
    phone_number:{
        type: String,
        required: false
    },
    fax:{
        type: String,
        required: false
    },
    contact_person:{
        type: String,
        required: false
    },
    postal_code:{
        type: String,
        required: false
    },
    VAT:{
        type: String,
        required: false
    },
    hidden:{
        type: Boolean,
        default: false
    },
})

mongoose.model('supplier', SupplierSchema)