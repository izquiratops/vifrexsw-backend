const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    reference:{
        type: String,
        required: true
    },
    supplier_code:{
        type: String,
        required: true
    },
    date:{
        type: String,
        required: true
    },
    client:{
        type: String,
        required: true
    },
    company:{
        type: String,
        required: true
    },
    department:{
        type: String,
        required: true
    },
    hidden:{
        type: Boolean,
        default: false
    },
})

mongoose.model('finalorder', OrderSchema)