const mongoose = require('mongoose')
const Schema = mongoose.Schema

const FabricSchema = new Schema({
    supplier:{
        type: String,
        required: true
    },
    construction:{
        type: String,
        required: true
    },
    type:{
        type: String,
        required: true
    },
    fabric_supplier_code:{
        type: String,
        required: true
    },
    width:{
        type: String,
        required: true
    },
    finishing:{
        type: String,
        required: false
    },
    comments:{
        type: String,
        required: false
    },
    weight:{
        type: String,
        required: true
    },
    price:{
        type: String,
        required: false
    },
    vifrex_price:{
        type: String,
        required: false
    },
    date:{
        type: String,
        required: true
    },
    vifrex_id:{
        type: String,
        required: true
    },
    composition:{
        type: String,
        required: true
    },
    fabric_description:{
        type: String,
        required: false
    },
    hidden:{
        type: Boolean,
        default: false
    },
})

mongoose.model('fabric', FabricSchema)