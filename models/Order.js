const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    reference:{
        type: String,
        required: true
    },
    client_company:{
        type: String,
        required: true
    },
    client_department:{
        type: String,
        required: true
    },
    client_name:{
        type: String,
        required: true
    },
    date:{
        type: String,
        required: true
    },
    hidden:{
        type: Boolean,
        default: false
    },
})

mongoose.model('order', OrderSchema)