# Notas de desarrollo
Para tirar el server solo necesitas escribir `node app` ya que app.js es el main script. 

De todas formas, si cambias algo node no lo refrescará on the fly. Instala `sudo npm install -g nodemon` y tira el server con `nodemon app` para que a cada cambio guardado el server se refresque solito.

**Pasos**
1) Asegurate de que tienes mongodb
2) `npm install`
3) `nodemon app`
4) Prueba de meter datos con un POST (Yo uso Curl)

`curl --request POST -d '{"key1":"value1", "key2":"value2"}' -H "Content-Type: application/json" http://localhost:8180/post_data`

5) El POST te devuelve la ID al que pertenece el objeto en la bbdd, búscalo con un GET

`curl -d 'id=<<AQUÍ VA LA ID>>' -X GET http://localhost:8180/get_data`

### Mongo things
https://developer.mozilla.org/es/docs/Learn/Server-side/Express_Nodejs/mongoose

### CURL 101
https://devhints.io/curl
https://www.keycdn.com/support/popular-curl-examples

## Express gists
### Mostrar un HTML 
`main.get('/', (req, res) => res.sendFile('index.html', { root: __dirname }))`

### Msg for testing purposes
`main.get('/', (req, res) => res.send('Hey there!'))`

## Posts de interés
### How to process POST data in Node.js?
https://stackoverflow.com/questions/4295782/how-to-process-post-data-in-node-js

### Read from an HTML Form
https://stackoverflow.com/questions/9304888/how-to-get-data-passed-from-a-form-in-express-node-js

### Mongo Command Helpers
https://docs.mongodb.com/manual/reference/mongo-shell/
